export default {
  init() {
    // JavaScript to be fired on all pages
    $(document).foundation();
    if(typeof skrollr != "undefined"){
      var s = skrollr.init({
        edgeStrategy: 'set',
        easing: {
          WTF: Math.random,
          inverted: function(p) {
            return 1-p;
          }
        }
      });
    }

    initAnimations();

    var vid = document.getElementById("video"); 

    $('.media-container .play-button').click(function(e){
      // e.stopPropagation();
      if($(this).data('state') == 'pause'){
        vid.play();
        $(this).data('state', 'play').toggleClass('hidden');
      }
      else{
        vid.pause();
        $(this).data('state', 'pause').toggleClass('hidden');

      }
    });

    $('.media-container').click(function(){
      vid.pause();
      $(this).find('.play-button').data('state', 'pause').removeClass('hidden');
    })

    $('.logo').click(function(){
      $([document.documentElement, document.body]).animate({
        scrollTop: 0
      }, 800);
    });

    $('.hamburger').click(function(){
      $('.mobile-menu').addClass('visible');
    });

    $('.mobile-menu .exit').click(function(){
      $('.mobile-menu').removeClass('visible');
    });

    if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {           
      var iphoneScrollTo = $('.blog .big-text').offset().top + Number($('body').scrollTop());
    }

    $('.intro .go-lower').click(function(){
      $('html, body').animate({
        scrollTop: iphoneScrollTo
      }, 800);
    });

    $('.home .go-lower').click(function(){
      if(s){
        var scrollBy = 2400 - document.documentElement.scrollTop ;
        window.scrollBy(0,scrollBy);
      }

      else{

        if(iphoneScrollTo){
          $('body').css('overflow', 'visible').animate({
            scrollTop: iphoneScrollTo
          }, 800);
        }
        else{
          $('html, body').animate({
            scrollTop: $('.blog .big-text').offset().top + Number($('body').scrollTop())
          }, 800);
        }
      }
    });

    // $('.single-post .video .go-lower').click(function(){
    //   var elem = $(this);
    //   $([document.documentElement, document.body]).animate({
    //     scrollTop: elem.parents('.video').closest('.text-container').offset().top
    //   }, 800);
    // });

    $('.consent label .rdm-button').click(function(e){
      $(this).css('display','none');
      $(this).parents('label').find('.readmore').css('height','auto');
    });

    $('.consent label .rdm-button-collapse').click(function(e){
      $(this).parents('.readmore').css('height','0').parents('label').find('.rdm-button').css('display','inline-block');
    });



    $('.mc4wp-form .btn').click(function(e){
      var form = $(this).parents('form.mc4wp-form');
        validateNewsletterAcceptances(form);
    });

    $('.mc4wp-form input[type="email"]').bind('keyup', function(e) {
      var form = $(this).parents('form.mc4wp-form');
      if ( e.keyCode === 13 ) {  
        validateNewsletterAcceptances(form);  
      }
  
  });

    function validateNewsletterAcceptances(form){
      form.find('.mc4wp-response').find('.acceptances').css('display','none');
      if(!form.find('#newsletterAcceptance').is('checked') || !form.find('#newsletterAcceptance2').is('checked')){
        form.find('.mc4wp-response').html('<span class="acceptances">Aby zapisać się do newslettera proszę zaznacz obie zgody</span>');
      }
    }


    function setPostsSlider(){
      if($(window).width() < 1000){
        $('.blog .posts-holder').slick({
          slidesToShow: 1,
          dots:true,
          arrows:false,
          autoplay:true,
          autoplaySpeed: 4000,
        });
      }
  
      else{
        $('.blog .posts-holder.slick-slider').slick('unslick');
      }
    }

    function initAnimations(){
      if($(window).width() <= 1000){
        if(!$('body').data('aos-easing')){
          AOS.init();
        }
        $('.intro-elements').css('height', $(window).height());
      }
    }

    setPostsSlider();

    $( window ).resize(function() {
      setPostsSlider();
      initAnimations();
    });

    $(window).scroll(function(){
      if($(window).width() > 1000){
        if($(window).scrollTop() >= 1300){
          vid.play();
          $('.media-container .play-button').data('state', 'play').addClass('hidden');
        }
        if($(window).scrollTop() >= 2400 || $(window).scrollTop() <= 1100){
          vid.pause();
          $('.media-container .play-button').data('state', 'pause').removeClass('hidden');
        }
      }
    })

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
