<?php

$context = Timber::get_context();
$context['post'] = Timber::get_post();
$context['related'] = Timber::get_posts(array(
  'numberposts' => 3,
));

$template = wp_is_mobile() ? 'front-page-mobile' : 'front-page';
Timber::render( 'views/templates/'.$template.'.twig', $context );
