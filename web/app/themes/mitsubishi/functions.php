<?php
require_once( __DIR__ . '/vendor/autoload.php' );
show_admin_bar(false );

add_action('wp_head','render_meta_title');
add_action( 'init', 'register_navigation' );
add_action('admin_init', 'change_tinyMCE_styles');


$timber = new \Timber\Timber();

$sage_includes = [
  'lib/timber.php',
  'lib/assets.php',
	'lib/setup.php',
];

foreach ( $sage_includes as $file ) {
  $filepath = locate_template( $file );
	if ( !$filepath  ) {
		trigger_error( sprintf( __( 'Error locating %s for inclusion', 'sasquatch' ), $file ), E_USER_ERROR );
	}

	require_once $filepath;
}

// shift + space to add &nbsp; in wysiwyg editor
add_filter( 'tiny_mce_before_init', function( $initArray ) {
	$initArray['setup'] = "<script>
 [function(editor) {
	var down = {};
	editor.onKeyDown.add( function( editor, event ) {
	down[event.keyCode] = true;
	});
	editor.onKeyUp.add( function( editor, event ) {
	if ( down[16] && down[32] ) {
	editor.execCommand( 'mceInsertContent', false, '&nbsp;' ); // inserts non-breaking space
	}
	down[event.keyCode] = false;
	});
 
 }][0]
 </script>";
	$initArray['entities'] = '160,nbsp,38,amp,60,lt,62,gt'; 
	$initArray['entity_encoding'] = 'named';
	return $initArray;
 });

function register_navigation() {
	register_nav_menu('header-menu',__( 'Header Menu' ));
}

function render_meta_title(){
	?>
	<title><?php bloginfo('name'); ?><?php wp_title(); ?></title>
	<?php
}


function remove_default_format_select( $buttons ) {
	//Remove the format dropdown select and text color selector
	$remove = array( 'formatselect' );

	return array_diff( $buttons, $remove );
}

function my_new_mce_buttons( $buttons ) {
	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}


function change_tinyMCE_styles(){

	if(check_admin_post_type('post') || check_post_template('tpl-about.php') ){
		// Register our callback to the appropriate filter
		add_filter( 'mce_buttons', 'remove_default_format_select' );
		add_filter( 'mce_buttons', 'my_new_mce_buttons' );
		add_action( 'admin_init', 'wpdocs_theme_add_editor_styles' );
		add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );
		add_action('admin_enqueue_scripts', 'register_admin_custom_styles');
	}
}

function check_admin_post_type($type){
	if($_GET['post']){
		if(get_post_type($_GET['post']) == $type){
			return true;
		}
	}
	return false;
}

function check_post_template($template){
	if($_GET['post']){
		if(get_post_meta($_GET['post'], '_wp_page_template', true) == $template){
			return true;
		}
	}
	return false;
}



// Callback function to filter the MCE settings
function my_mce_before_init_insert_formats( $init_array ) {  
	// Define the style_formats array
	$style_formats = array(
						array(
							'title' => 'Akapit',
							'block' => 'p',
							),
					array(
							'title' => 'Duży header',
							'block' => 'h3'
							),
					array(
							'title' => 'Czerwony pasek',
							'block' => 'h5'
							)
			);
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );  

	return $init_array;  

} 
// Attach callback to 'tiny_mce_before_init' 

/**
 * Registers an editor stylesheet for the theme.
 */
function wpdocs_theme_add_editor_styles() {
	add_editor_style( 'custom-editor-style.css' );
}

function register_admin_custom_styles(){
	wp_enqueue_style('binar-admin-styles', get_template_directory_uri().'/binar-admin-panel.css');
}