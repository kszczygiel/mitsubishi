<?php
/*
Template Name: O nas
*/

$context = Timber::get_context();
$context['post'] = Timber::get_post();

$template = wp_is_mobile() ? 'about-mobile' : 'about';
Timber::render( 'views/templates/'.$template.'.twig', $context );

?>
