<?php

$args = array(
  'post_type' => 'post',
  'numberposts' => '3'
);

$context = Timber::get_context();
$context['post'] = Timber::get_post();
$context['related'] = Timber::get_posts($args);
// $context['prev'] = get_previous_post();
// $context['next'] = get_next_post();
// Timber::render( 'views/templates/single.twig', $context );
$template = wp_is_mobile() ? 'single-mobile' : 'single';
Timber::render( 'views/templates/'.$template.'.twig', $context );
